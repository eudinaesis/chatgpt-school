---
title: "Barriers to Using Spaced-Repetition Recall Apps in Secondary School Language Classes"
subtitle: "Intermediate Presentation"
author: Peter Northup
date: 14.12.2022
lang: en
width: 1920
height: 1080
theme: white
bibliography: /Users/pnorthup/Documents/bibs/srs.json
nocite: |
    @*
---

# Background

## Formalities

- Title: "Barriers to Using Spaced-Repetition Recall Apps in Secondary School Language Classes"
- Advisor: Renate Motschnig

## Context & Motivation

- There is a large and well-replicated evidence-base showing that recall exercises using "spaced repetition" (SR) aids memorization, especially over time
- Digitalization in schools and society means that every student can use SR apps on their own devices (e.g. apps/webapps on smartphones)
- ... but I believe these tools are not used very often, even in language classes where rote memorization is important and unavoidable

# Research question & approach

## Research question: why not?

1- Why don't (more) language teachers use these tools?

2- What would help overcome these barriers?

## Hypotheses

1. Unfamiliarity
   - Teachers unaware of the research
   - Teachers don't know appropriate apps
2. Policy/Data protection concerns
3. Too much effort, especially if doing it alone

## Methods

- Semi-structured interviews with a few language teachers in order to develop hypotheses further and guide potential solutions
- Develop instructional materials for teachers to
  - Very briefly summarize benefits of SR
  - Explain available apps/tools
  - Clarify issues with GDPR
- Larger online survey to assess
  - Reasons why teachers aren't using
  - Whether informational materials might help

# Plan

## Done

- [x] Literature search
- [x] Reach out to convenience sample of language teachers to get 

## In Progress

- [ ] Research GDPR issues
- [ ] Assess available apps/services

## To Do

- [ ] Write instructional materials
- [ ] Write survey for larger sample
- [ ] Ask Bildungsdirektion for permission to send to large sample of language teachers
  - [ ] If not, use Facebook teacher groups to recruit participants
- [ ] Send out survey
- [ ] Evaluate results, write up paper

## Problems

# Thank you!

## Literature
