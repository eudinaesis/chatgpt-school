---
title: "AI Text-generation in Schools"
subtitle: "Challenges and Opportunities"
author: Peter Northup
date: 23.01.2023
lang: en
width: 1280
height: 800
revealjs-url: ../../../Developer/reveal.js
theme: white
bibliography: ./chatgpt.json
nocite: |
    @*
---

# The headlines: ChatGPT will change everything!

## Both in English-language media ...

::: scrollok

![](./media/headlines-en.png){ width=100% }

:::

## ... and in German

::: scrollok 

![](./media/headlines-de.png){ width=100% }

:::

## Beyond the hype

- What is it?
- What can it do?
- What do teachers need to know NOW?
- How should things change teaching in the medium-/long-term?

# Background: ChatGPT & generative AI

## What is ChatGPT?

![](./media/what-is-gpt.png)

## How does it work?

![](./media/how-do-you-work.png)

## What can it do?

::: scrollok

![](./media/what-can-it-do.png)

:::

## Other generative AI projects

- Translation: DeepL
- Coding: Gitlab CoPilot 
- Images from text: Dall-E, Craiyon, Stable Diffusion, &c.
- Videos: Synthesia

## "Only the tip of the iceberg"

::: scrollok

![The Generative AI Startup Landscape, by Ollie Forsyth, Antler](./media/antler-Generative-AI-Landscape_v2.png)

:::

# Why should teachers care?

## Immediate relevance

- Revolutionizes cheating
- Need to rethink purpose of writing assignments
- ... and all homework
- Student motivation: "Why learn this, if I can just have ChatGPT do it?"
- Important to explicitly address it as a topic in its own right

## Concrete example: job applications

::: scrollok

![](./media/job-app.png)

:::

## Homework assignments of this kind are now obsolete: now what?

- Write everything in class?
- More "flipped classroom"?

## Concrete example: programming

::: scrollok

![](./media/python-ex1.png)

:::

## Convincing but wrong?

::: scrollok

![](./media/python-ex2.png)

:::

## Note: the programming quality will get much better!

- No more at-home programming projects? :sob: 
- Surveillance in computer labs to block this? :scream:
- Do we want to be police, or teachers? :thinking:

## Short-term defensive responses

- Be aware of AI's capabilities
- Rethink homework entirely
- Flipped classroom
- Discuss rules for use, be honest and open with classes

# Beyond "playing defense"

## How does generative AI change the world schools are preparing students for?

- Changes what competences we want kids to have
- The nature of writing itself in society may shift 
- Shift towards editing, combining outputs and "herding" AIs

## Rethinking job application assignment

- How will our students *actually* write job applications?
    - They will use AI assistance!
- How can we best prepare them to use it?
    - Effectively
    - Efficiently
    - Ethically
- Example: [Philippe Wampfler, "Umgang mit KI-Programmen im Schreibunterricht"](https://schulesocialmedia.com/2022/10/15/grundlagenartikel-umgang-mit-ki-programmen-im-schreibunterricht/)

## Informatics: Rethinking coding assignments

:::::::::::::: {.columns}
::: {.column width="60%"}

How will our students actually write code?

:::
::: {.column width="40%"}

![](./media/copypaste-so.png){width="40%" height="100%"}

:::
::::::::::::::

## Students will need to *use* AI when coding!

- What are the competences we want them to have?
- How do these translate into learning goals?
- What assignments actually achieve these?
- Overall approach: AI as a programming partner
- Goal: "from code monkey to software architect"

# Opportunities for teachers

## Produce assignments, questions, input texts

::: scrollok

![](./media/history-question.png){ width=100% }

:::

## Assignments & grading rubrics

::: scrollok

![](./media/recursion-rubric.png){ width=100% }

:::

# Resources

## Helpful overviews and collections of links

[Herft, Gagné, König: Leitfaden für Lehrkräfte](https://ugc.padletcdn.com/uploads/padlet-uploads/5632542/3427599b398cf86be7ad691d2a52fe6e/ChatGPT_Leitfaden_fuer_Lehkraefte_DE_1.pdf?token=YqXTi7OBdZ61swz7wJ_eaq2IbfXAozT_xy_UTi4JrzgQ5X3Cr2hMhg4bj0SFulWolLD3bimOs5KzUOYSTVwNEq1AGo1d2T99Y0ESWQQDPOoOt2S-dJ0QnmTq1Z4Ks5n1Y-ofzhOG3VT-e--1JY-fRQh_ce0go2vtb8xwd7Z3F9LxkNpq2eJbqf5_c96wakXKF41bN133f3q8VHEVJath9BNuSDR1yZg-_Nn7w4HNdiwFyRcUbAlbA4w4CMWmBYK3TxceS1RMCDn2QWNu51Ciwg==)

[Beat Döbeli Honegger: ChatGPT & Schule](https://mia.phsz.ch/bin/diff/MIA/ChatGPT?rev=27)

[Matt Miller: ChatGPT, Chatbots and Artificial Intelligence in Education](https://ditchthattextbook.com/ai/)

[Alicia Bankhofer: DE/EN Padlet](https://padlet.com/aliciabankhofer/ki-f-r-den-unterricht-links-ideen-ressourcen-einsatzideen-f--7qczpkgbgpthugn)

[This presentation](https://eudinaesis.gitlab.io/chatgpt-school) and [as standalone HTML page](https://eudinaesis.gitlab.io/chatgpt-school/pnorthup-chatgpt.html)

## Further reading
