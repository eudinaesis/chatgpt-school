# index.html is normal revealjs
# [filename].html is embed-resources revealjs
# .pdf is embed-resources revealjs
# make [all] does all 3, copies the media to public/media

all: public/index.html public/pnorthup-digmed-chatgpt.pptx public/pnorthup-chatgpt.html

public/index.html: pnorthup-chatgpt.md style.css
	pandoc --from markdown+emoji --standalone --citeproc --verbose --css style.css --embed-resources -t revealjs $< -o $@

public/pnorthup-chatgpt.html: pnorthup-chatgpt.md
	pandoc --from markdown+emoji --standalone --citeproc --verbose --embed-resources $< -o $@

public/pnorthup-digmed-chatgpt.pptx: pnorthup-chatgpt.md style.css
	pandoc --from markdown+emoji --standalone --citeproc --embed-resources $< -o $@
